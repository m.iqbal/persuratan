<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class department extends Model
{
    protected $fillable = [
        'name', 'information',
    ];

    public function user(){
        return $this->hasMany('App\Models\User');
    }

    public function incoming_mail(){
        return $this->belongsToMany('App\Models\incoming_mail');
    }
}
