<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username','role', 'department_id','division_id','section_id', 'password',
    ];

    public function department(){
        return $this->belongsTo('App\Models\department');
    }

    public function division(){
        return $this->belongsTo('App\Models\division');
    }

    public function section(){
        return $this->belongsTo('App\Models\section');
    }

    public function incoming_mail(){
        return $this->belongsToMany('App\Models\incoming_mail');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
