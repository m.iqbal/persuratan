<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class index extends Model
{
    protected $fillable = [
        'code', 'name', 'information'
    ];

    public function incoming_mail(){
        return $this->hasMany('App\Models\incoming_mail');
    }
}
