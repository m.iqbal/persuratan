<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class follow_up extends Model{
    protected $fillable = [
        'job_desc', 'incoming_mail_id'
    ];

    public function incoming_mail(){
        return $this->belongsTo('App\Models\incoming_mail');
    }
}
