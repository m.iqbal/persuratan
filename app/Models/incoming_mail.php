<?php

namespace App\Models;

use App\Models\department;

use App\Models\division;

use App\Models\section;

use App\Models\user;

use App\Models\index;

use Illuminate\Database\Eloquent\Model;

class incoming_mail extends Model
{
    protected $fillable = [
        'mail_date',
        'mail_code',
        'mail_no',
        'sender',
        'contact',
        'mail_subject',
        'mail_info',
        'completion_status',
        'intended_disposition',
        'index_id',
        'jobdesc',
        // 'user_id',
        // 'department_id',
        // 'division_id',
        // 'section_id',
    ];

    public function index(){
        return $this->belongsTo('App\Models\index');
    }

    public function user(){
        return $this->belongsToMany('App\Models\User');
    }

    public function department(){
        return $this->belongsToMany('App\Models\department');
    }

    public function division(){
        return $this->belongsToMany('App\Models\division');
    }

    public function section(){
        return $this->belongsToMany('App\Models\section');
    }
}
