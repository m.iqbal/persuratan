<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\section;

class SectionController extends Controller
{
    public function get(Request $request){
        $data = section::get();
        $userLogin = Auth::User();

        return view('admin/list-section',[
            'sections' => $data,
            'userLogin'=> $userLogin
        ]);
    }

    public function store(Request $request){
        $data = $request->all();
        $section = section::create([
            'name' => $data['nama_seksi'],
            'information' => $data['keterangan'],
        ]);

        return back();
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $section = section::find($id);

        $section -> nama   = $data['nama_seksi'];
        $section -> nama   = $data['keterangan'];

        $section->save();
        return back();
    }

    public function destroy($id){
        $section = section::find($id);
        $section->delete();
        return back();
    }
}
