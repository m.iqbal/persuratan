<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\incoming_mail;

use App\Models\index;

use App\Models\department;

use App\Models\division;

use App\Models\section;

use App\Models\User;

use DB;

class MailController extends Controller
{
    public function get(){
        $data = incoming_mail::get();
        $department = department::get();
        $divison = division::get();
        $section = section::get();
        $index   = index::get();
        $userLogin = Auth::user();        

        return view('surat-masuk',[
            'mails' => $data,
            'departments' => $department,
            'divisions' => $divison,
            'sections'  => $section,
            'indices'   => $index,
            'userLogin' => $userLogin,            
        ]);
    }

    public function get2(){
        $data = incoming_mail::get();
        $department = department::get();
        $divison = division::get();
        $section = section::get();
        $index   = index::get();
        $user    = User::get();
        $userKadin = User::where('role','Kepala Dinas')->first();
        $userSekre = User::where('role','Sekretaris')->first();
        $userLogin = Auth::user();

        return view('umpeg/surat-masuk',[
            'mails' => $data,
            'departments' => $department,
            'divisions' => $divison,
            'sections'  => $section,
            'indices'   => $index,
            'userLogin' => $userLogin,
            'userkadin' => $userKadin,
            'usersekre' => $userSekre,    
            'users'     => $user
        ]);
    }

    public function getSuratku(){
        $userLogin = Auth::user();
        $userKadin = User::where('role','Kepala Dinas')->first();
        $userSekre = User::where('role','Sekretaris')->first();
        $staffUmpeg = DB::table('users')
                    ->join('divisions','divisions.id','=','users.division_id')
                    ->select('users.id','users.name')
                    ->where('users.role','Staff')
                    ->where('divisions.name', 'Umum Kepegawaian')
                    ->get();

        if($userLogin->role == 'Kepala Bidang'){
            $department_id = $userLogin->department_id;
            $department = department::find($department_id);

            $mail = $department->incoming_mail()->get();
        }elseif($userLogin->role == 'Kepala Sub Bagian'){
            $division_id = $userLogin->division_id; 
            $division = division::find($division_id);

            $mail = $division->incoming_mail()->get();
        }elseif($userLogin->role == 'Kepala Dinas'){
            $user_id = $userLogin->id;
            $user = User::find($user_id);

            $mail = $user->incoming_mail()->get();
        }else{
            $user_id = $userLogin->id;
            $user = User::find($user_id);

            $mail = $user->incoming_mail()->get();
        }
        
        $department = department::get();
        $division   = division::get();
        $section    = section::get();
        $user       = User::get();

        return view('suratku', [
            'mails'      => $mail,
            'departments'=> $department,
            'divisions'  => $division,
            'sections'   => $section,               
            'userLogin'  => $userLogin,
            'userkadin' => $userKadin,
            'usersekre' => $userSekre, 
            'users'     => $user,
            'staffumpeg'=> $staffUmpeg
        ]);
    }
    
    public function store(Request $request){
        $data = $request->all();
        $date = date('Y-m-d', strtotime($data['tglSurat']));

        $mail = incoming_mail::create([
            'mail_date'=> $date,
            'mail_no'   => $data['nomorSurat'],
            'mail_code' => $data['kodeSurat'],
            'sender'    => $data['asalSurat'],
            'contact'   => $data['kontak'],
            'mail_subject' => $data['perihal'],
            'mail_info' => $data['keterangan'],
            'index_id'  => $data['indexDisposisi'],
        ]);

        return back();
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $date = date('Y-m-d', strtotime($data['tglSurat']));
        $mail = incoming_mail::find($id);

        $mail->mail_no   = $data['nomorSurat'];
        $mail->mail_code = $data['kodeSurat'];
        $mail->sender    = $data['asalSurat'];
        $mail->mail_date = $date;
        $mail->contact   = $data['kontak'];
        $mail->mail_subject = $data['perihal'];
        $mail->mail_info    = $data['keterangan'];
        $mail->index_id     = $data['indexDisposisi'];

        $mail->save();
        return back();
    }

    public function destroy($id){
        $mail = incoming_mail::find($id);
        $mail->delete();
        return back();
    }

    public function jobdesc(Request $request, $id){
        $data = $request->all();
        $mail = incoming_mail::find($id);
        
        $mail -> jobdesc = $data['jobdesc'];
        $mail->save();
   
        return back();
    }

    public function dispo(Request $request, $id){
        $data = $request->all();
        $mail = incoming_mail::find($id);
        $userLogin = Auth::user();

        $mail->intended_disposition = $data['tujuan'];
    
        if($data['tujuan'] == 'bidang'){
            $department = department::find($data['bidangDisposisi']);

            if(!$department->incoming_mail()->find($mail->id)){
                $department->incoming_mail()->save($mail, ['status'=>'Tindak Surat']);                
            }
            
            $mail -> status_department = true;
        }else if($data['tujuan'] == 'sub bagian'){
            $division = division::find($data['bagianDisposisi']);

            if(!$division->incoming_mail()->find($mail->id)){
                $division->incoming_mail()->save($mail, ['status' => 'Tindak Surat']);                
            }

             $mail -> status_division = true;
        }else if($data['tujuan'] == 'kasie'){
            $section = section::find($data['seksiDisposisi']);

            if(!$section->incoming_mail()->find($mail->id)){
                $section->incoming_mail()->save($mail, ['status' => 'Tindak Surat']);                
            }

            $mail -> status_section = true;
        }else{
            $user = user::find($data['tujuan']);

            if(!$user->incoming_mail()->find($mail->id)){
                $user->incoming_mail()->save($mail, ['status' => 'Tindak Surat']);                
            }

            if ($user->role == 'Kepala Dinas') {
                $mail -> status_kadin = true;
            } else if($user->role == 'Sekretaris'){
                $mail -> status_sekre = true;                
            }            
        }

        if($userLogin->department_id !=null){
            $department_id = $userLogin->department_id;
            $prevDepartment = department::find($department_id);

            $prevDepartment->incoming_mail()->updateExistingPivot($mail, ['status'=>'diteruskan']);
            $mail->status_department = false;
            
        }elseif($userLogin->division_id != null){
            $division_id = $userLogin->division_id;
            $prevDivision = division::find($division_id);

            $prevDivision->incoming_mail()->updateExistingPivot($mail->id, ['status' => 'diteruskan']);
            $mail->status_division = false;
            
        }else if($userLogin->section_id != null){
            $section_id = $userLogin->section_id;
            $prevSection = section::find($section_id);

            $prevSection->incoming_mail()->updateExistingPivot($mail->id, ['status' => 'diteruskan']);
            $mail->status_section = false;
            
        }else{
            $user_id = $userLogin->id;
            $prevUser = User::find($user_id);

            $prevUser->incoming_mail()->updateExistingPivot($mail->id, ['status' => 'diteruskan']);            
            if($prevUser->role == 'Kepala Dinas'){
                $mail->status_kadin = false;
            }else if($prevUser->role == 'Sekretaris'){
                $mail->status_sekre = false;
            }
        }

        $mail->save();
        return back();
    }

    public function completionDispo(Request $request, $id){
        $data = $request->all();
        $mail = incoming_mail::find($id);
        $mail ->completion_status = true;
        $mail->save();
        return back();
    }
}