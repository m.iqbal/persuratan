<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\User;

use App\Models\department;

use App\Models\division;

use App\Models\section;

use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    public function getDashboard(){
        $userLogin = Auth::user();        
        return view('home',[
            'userLogin' => $userLogin
        ]);
    }

    public function get(){
        $data = User::get();
        $userLogin = Auth::User();
        $department = department::get();
        $division = division::get();
        $section = section::get();

        if($userLogin->role != 'Admin'){
            alert()->error('Anda tidak diizinkan membuka halaman ini','Ditolak')->autoclose(3000);
            return redirect('/');
        }

        return view('admin/list-user',[
            'users' => $data,
            'userLogin' => $userLogin,
            'departments' => $department,
            'divisions'   => $division,
            'sections'    => $section,
        ]);
    }

    public function store(Request $request){
        $data = $request->all();
        
        if($data['role'] == "Kepala Sub Bagian"){
            $user = User::create([
                'name' => $data['nama'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
                'division_id' => $data['rolebagian'],
            ]);
        }else if($data['role'] == "Kepala Bidang"){
            $user = User::create([
                'name' => $data['nama'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
                'department_id' => $data['rolebidang'],
            ]);
        }else if($data['role'] == "Kasie"){
            $user = User::create([
                'name' => $data['nama'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
                'section_id' => $data['roleseksi'],
            ]);
        }else if($data['role'] == "Staff"){
            if($data['rolestaff'] == "seksi"){
                $user = User::create([
                    'name' => $data['nama'],
                    'username' => $data['username'],
                    'password' => bcrypt($data['password']),
                    'role' => $data['role'],
                    'section_id' => $data['rolestaffseksi'],
                ]);
            }else{
                $user = User::create([
                    'name' => $data['nama'],
                    'username' => $data['username'],
                    'password' => bcrypt($data['password']),
                    'role' => $data['role'],
                    'division_id' => $data['rolestaffsubag'],
                ]);
            }
        }else{
            $user = User::create([
                'name' => $data['nama'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
            ]);
        }

        return back();
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $user = user::find($id);        

        $user->role = $data['role'];        

        if($user['role']=="Kepala Bidang"){            
                $user->name = $data['nama'];
                $user->username = $data['username'];
                $user->password = bcrypt($data['password']);
                $user->role = $data['role'];
                $user->department_id = $data['rolebidang'];
                if($user->division_id != Null){
                    $user->division_id = Null;
                }else{
                    $user->section_id = NULL;
                }
        }else if($user['role']=="Kepala Sub Bagian"){            
            $user->name = $data['nama'];
            $user->username = $data['username'];
            $user->password = bcrypt($data['password']);
            $user->role = $data['role'];
            $user->division_id = $data['rolebagian'];
            if($user->department_id != Null){
                $user->department_id = NULL;
            }else{
                $user->section_id = NULL;
            }
        }else if($user['role']=="Kasie"){
            $user->name = $data['nama'];
            $user->username = $data['username'];
            $user->password = bcrypt($data['password']);
            $user->role = $data['role'];
            $user->section_id = $data['roleseksi'];
            if($user->division_id != Null){
                $user->division_id = NULL;
            }else{
                $user->department_id = NULL;
            }
        }else if($user['role']=="Staff"){
            $user->name = $data['nama'];
            $user->username = $data['username'];
            $user->password = bcrypt($data['password']);
            $user->role = $data['role'];
            if($data['rolestaff'] == "seksi"){
                $user->section_id = $data['rolestaffseksi'];
                if($user->division_id != Null){
                    $user->division_id = NULL;
                }else{
                    $user->department_id = NULL;
                }
            }else{
                $user->division_id = $data['rolestaffsubag'];
                if($user->section_id != Null){
                    $user->section_id = NULL;
                }else{
                    $user->department_id = NULL;
                }
            }
        }else{
            if($user['department_id'] != NULL){
                $user->name = $data['nama'];
                $user->username = $data['username'];
                $user->password = bcrypt($data['password']);
                $user->role = $data['role'];
                $user->department_id = NULL;
            }else if($user['division_id'] != NULL){
                $user->name = $data['nama'];
                $user->username = $data['username'];
                $user->password = bcrypt($data['password']);
                $user->role = $data['role'];
                $user->division_id = NULL; 
            }else if($user['section_id'] != NULL){
                $user->name = $data['nama'];
                $user->username = $data['username'];
                $user->password = bcrypt($data['password']);
                $user->role = $data['role'];
                $user->section_id = NULL; 
            }else{
                $user->name = $data['nama'];
                $user->username = $data['username'];
                $user->password = bcrypt($data['password']);
                $user->role = $data['role'];
            }
        }

        $user->save();
        return back();
    }

    public function destroy($id){
        $user = user::find($id);
        $user->delete();
        return back();
    }
}
