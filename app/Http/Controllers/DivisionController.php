<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\division;

class DivisionController extends Controller
{
    public function get(){
        $data = division::get();
        $userLogin = Auth::User();

        return view('admin/list-division',[
            'divisions' => $data,
            'userLogin' => $userLogin,
        ]);
    }

    public function store(Request $request){
        $data = $request->all();
        $division = division::create([
            'name' => $data['nama_subag'],
            'information' => $data['keterangan'],
        ]);

        return back();
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $division = division::find($id);

        $division -> nama   = $data['nama_subag'];
        $division -> nama   = $data['keterangan'];

        $division->save();
        return back();
    }

    public function destroy($id){
        $division = division::find($id);
        $division->delete();
        return back();
    }
}
