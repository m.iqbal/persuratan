<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\index;

class IndexController extends Controller
{
    public function get(){
        $data = index::get();
        $userLogin = Auth::user();

        return view('admin/list-index',[
            'indices' => $data,
            'userLogin' => $userLogin
        ]);
    }

    public function store(Request $request){
        $data = $request->all();
        $index = index::create([
            'code' => $data['kode'],
            'name' => $data['nama_index'],
            'information' => $data['keterangan']
        ]);

        return back();
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $index = index::find($id);

        $index -> code = $data['kode'];
        $index -> name = $data['nama_index'];
        $index -> information = $data['keterangan'];

        $index->save();
        return back();
    }

    public function destroy($id){
        $index = index::find($id);
        $index->delete();
        return back();
    }
}
