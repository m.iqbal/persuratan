<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\follow_up;

use App\Models\incoming_mail;

class FollowUpController extends Controller
{
    public function get(){
        $data = follow_up::get();
        $userLogin = Auth::user();

        return view('',[
            'follow_ups' => $data,
            'userLogin'  => $userLogin
        ]);
    }

    public function store(Request $request){
        $data = $request -> all();

        $followUp = follow_up::create([
            'job_desc' => $data['jobdesc'],
        ]);
        
        return back();
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $followUp = follow_up::find($id);
        
        $followUp -> job_desc = $data['jobdesc'];

        $followUp->save();
        return back();
    }
}
