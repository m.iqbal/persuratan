<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\department;

// use Alert;

class DepartmentController extends Controller
{
    public function get(){
        $data = department::get();
        $userLogin = Auth::user();

        // if($userLogin != 'Admin'){
        //     Alert::error('Anda tidak diizinkan membuka halaman ini','Ditolak');
        //     return redirect('/home');
        // }

        return view('admin/list-department',[
            'departments' => $data,
            'userLogin' => $userLogin,
        ]);
    }

    public function store(Request $request){
        $data = $request->all();
        $department = department::create([
            'name'          => $data['nama_bidang'],
            'information'   => $data['keterangan']
        ]);

        return back();
    }

    public function update(Request $request, $id){
        $data = $request->all();
        $department = department::find($id);

        $department->name           = $data['nama_bidang'];
        $department->information    = $data['keterangan'];

        $department->save();
        return back();
    }

    public function destroy($id){
        $department = department::find($id);
        $department->delete();
        return back();
    }
}
