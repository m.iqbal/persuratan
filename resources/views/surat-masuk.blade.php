@extends('layouts.layoutapp')

@section('title')
Sistem Informasi Manajemen Persuratan | Rekap surat masuk
@endsection

@section('sidebar-menu')
<link rel="stylesheet" href="{{URL('css/mail.css')}}">
<li>
    <a href="{{URL('/')}}">
        <i class="fa fa-home"></i> <span>Dashboard</span>
    </a>
</li>

<li class="active">
    <a href="{{URL('surat-masuk')}}">
        <i class="fa fa-download"></i>
        <span>Surat Masuk</span>
    </a>
</li>

<li>
    <a href="{{URL('surat-keluar')}}">
        <i class="fa fa-upload"></i>
        <span>Surat Keluar</span>
    </a>
</li>
@endsection

@section('content')
<section class="content-header" id="list-surat">
<div class="box">
    <div class="box-header">
        <h3>Rekap Surat Masuk</h3>
        <hr>        
        <a class="btn btn-success" id="button">
        <i class="fa fa-plus-square"></i>
        Tambah Data</a>
    </div>
    <div class="box-body">
        <table id="datatable">
          <thead>
            <tr class="active">
              <th>No.</th>
              <th>Tanggal Masuk Surat</th>
              <th>Kode Surat</th>
              <th>Nomor Surat</th>
              <th>Asal Surat</th>
              <th>Tanggal Surat</th>
              <th>Status</th>
              <th>Perihal</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($mails as $index => $mail)
            <tr>
              <td>{{$index+1}}</td>
              <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->created_at))->format('d F Y') }}</td>
              <td>{{$mail->mail_code}}</td>
              <td>{{$mail->sender}}</td>
              <td>{{$mail->mail_no}}</td>
              <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->mail_date))->format('d F Y') }}</td>
              @if($mail->completion_status == true)
                <td>Selesai</td>
              @else
                <td>Dalam Proses</td>
              @endif
              <td>{{$mail->mail_subject}}</td>                
                <td class="btn-action">
                    <a class="btn btn-info" onclick="detailData ( {{$mail->id}} )">Detail</a>
                    <a class="btn btn-warning" onclick="editData ( {{$mail->id}} )">Edit</a>
                    <a class="btn btn-danger" onclick="hapusData ( {{$mail->id}} )">Hapus</a>
                </td>             
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
</section>

<!-- tambah data -->
<section class="content" id="form-surat">
    <div class="box">
        <div class="box-header">
            <h3>Tambah Surat</h3>
            <hr>
        </div>
        <div class="box-body">
            <form action="{{ URL('surat-masuk')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
                <div class="form-group header-group-0" id="">
                    <label >
                        Nomor surat
                    </label>
                    <input type="text" required class="form-control" name="nomorSurat" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Kode surat (diisi dari Dinas Pendidikan Kab. Sleman)
                    </label>
                    <input type="text" required class="form-control" name="kodeSurat" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Asal surat
                    </label>
                    <input type="text" required class="form-control" name="asalSurat" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Tanggal surat
                    </label>
                    <input type="text" required readonly class="form-control datepicker" name="tglSurat" id="">
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Kontak
                    </label>
                    <input type="text" required class="form-control" name="kontak" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Perihal
                    </label>
                    <input type="text" required class="form-control" name="perihal" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Keterangan
                    </label>
                    <input type="text" required class="form-control" name="keterangan" id="">
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Index Disposisi
                    </label>
                    <select name="indexDisposisi" class="form-control" id="indexdispo">
                        @foreach($indices as $dIndex)
                        <option value="{{$dIndex->id}}">{{$dIndex->code}} - {{$dIndex->name}}</option>
                        @endforeach
                    </select>

                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>

 @foreach($mails as $mail)
<!-- detail data -->
<section class="content-header form-detail" id="detail-{{$mail->id}}">
    <div class="box">
        <div class="box-header">
            <h3>Detail Data Surat Masuk</h3>
            <hr>
        </div>
        <div class="box-body">
            <table>
                <tbody class="table-detail">
                <tr>
                    <td>Tanggal Masuk Surat</td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->created_at))->format('d F Y') }}</td>
                </tr>
                <tr>
                    <td>Kode Surat</td>
                    <td>{{$mail->mail_code}}</td>
                </tr>
                <tr>
                    <td>Nomor Surat</td>
                    <td>{{$mail->mail_no}}</td>
                </tr>
                <tr>
                    <td>Asal Surat</td>
                    <td>{{$mail->sender}}</td>
                </tr>
                <tr>
                    <td>Tanggal Surat</td>
                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->mail_date))->format('d F Y') }}</td>
                </tr>
                <tr>
                    <td>Kontak</td>
                    <td>{{$mail->contact}}</td>
                </tr>
                <tr>
                    <td>Perihal</td>
                    <td>{{$mail->mail_subject}}</td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td>{{$mail->mail_info}}</td>
                </tr>
                <tr>
                    <td>Kode Disposisi</td>
                    <td>{{$mail->index->code}}</td>
                </tr>
                <tr>
                    <td>Index Disposisi</td>                    
                    <td>{{$mail->index->name}}</td>                    
                </tr>
                <tr>
                    <td>Bidang Disposisi</td>
                    @if($mail->user_id != null)
                        <td>{{ $mail->user_id }}</td>
                    @elseif($mail->department_id != null)
                        <td>{{ $mail->department->name }}</td>
                    @elseif($mail->section_id != null)
                        <td>{{ $mail->section->name }}</td>
                    @elseif($mail->division_id != null)
                        <td>{{ $mail->division->name }}</td>
                    @else
                        <td> - </td>
                    @endif
                </tr>                
                <tr>
                    <td>Status Disposisi</td>
                    @if($mail->completion_status == true)
                        <td>Selesai</td>
                    @else
                        <td>Dalam Proses</td>
                    @endif
                </tr>
                </tbody>                
            </table>
            <div class="padding-btn">
                <a class="big-btn btn-danger">Kembali</a>   
            </div>
        </div>
    </div>
</section>

<!-- edit data -->
<section class="content form-edit" id="edit-{{ $mail->id }}">
    <div class="box">
        <div class="box-header">
            <h3>Edit Surat</h3>
            <hr>
        </div>
        <div class="box-body">
            <form action="{{ URL('surat-masuk/'.$mail->id.'/edit')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
                <div class="form-group header-group-0" id="">
                    <label >
                        Kode surat (diisi dari Dinas Pendidikan Kab. Sleman)
                    </label>
                    <input type="text" value="{{$mail->mail_code}}" required class="form-control" name="kodeSurat" id="">                    
                </div>
                
                <div class="form-group header-group-0" id="">
                    <label >
                        Nomor surat
                    </label>
                    <input type="text" value="{{$mail->mail_no}}" required class="form-control" name="nomorSurat" id="nomorsurat">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Asal surat
                    </label>
                    <input type="text" value="{{$mail->sender}}" required class="form-control" name="asalSurat" id="asalsurat">                    
                </div>
                
                <div class="form-group header-group-0" id="">
                    <label >
                        Tanggal surat
                    </label>
                    <input type="date" value="{{$mail->mail_date}}" required class="form-control datepicker" name="tglSurat" id="tglsurat">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Kontak
                    </label>
                    <input type="text" value="{{$mail->contact}}" required class="form-control" name="kontak" id="kontak">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Perihal
                    </label>
                    <input type="text" value="{{$mail->mail_subject}}" required class="form-control" name="perihal" id="perihal">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Keterangan
                    </label>
                    <input type="text" value="{{$mail->mail_info}}" required class="form-control" name="keterangan" id="ket">
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Index Disposisi
                    </label>
                    <select class="form-control" name="indexDisposisi" id="indexdispo">
                        @foreach($indices as $data)
                            <option value="{{$data->id}}">{{$data->kode}} - {{$data->nama}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>

<!-- hapus data -->
<form action="{{ URL('surat-masuk/'.$mail->id).'/delete' }}" class="form-hapus" id="hapus-{{ $mail->id }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">    
</form>

@endforeach 

<script>
    $('.datepicker').datepicker({  
        autoclose: true,
        format: 'dd-mm-yyyy'
    });

    $('#datatable').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    })

    $("#button").click(function(){
        $("#list-surat").hide();
        $("#form-surat").show();
    });

    $(".btn-danger").click(function(){
        $("#list-surat").show();
        $("#form-surat").hide();
    });

    function detailData(id){
        $(".form-detail#detail-"+id).show();
        $("#list-surat").hide();
    }

    $(".btn-danger").click(function(){
        $(".form-detail").hide();
        $("#list-surat").show();
    });

    function editData(id){
        $("#list-surat").hide();
        $(".form-edit#edit-"+id).show();
    };
  
    $(".btn-danger").click(function(){
        $(".form-edit").hide();
        $("#list-surat").show();
    });

    function dispoSurat(id){
        $("#dispo-surat-"+id).show();
        $("#list-surat").hide();
    }

    function hapusData(id){
        $(".form-hapus#hapus-"+id).submit();
    }

    function gantiPilihan(id){
        var tujuan = $('#pilihandispo-'+id).val();
        console.log('tujuan ke = '+tujuan+' dengan id = '+id)

        if(tujuan == 'bidang'){
            $('#selectBidangDispo-'+id).show();
            $('#selectBagianDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).hide();
            $('#selectBidangDispo-'+id).prop('disabled', false);
        }else if(tujuan == 'sub bagian'){
            $('#selectBagianDispo-'+id).show();
            $('#selectBidangDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).hide();
            $('#selectBagianDispo-'+id).prop('disabled', false);
        }else if(tujuan == 'kasie'){
            $('#selectSeksiDispo-'+id).show();
            $('#selectBagianDispo-'+id).hide();
            $('#selectBidangDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).prop('disabled', false);
        }else{
            $('#selectBidangDispo-'+id).hide();
            $('#selectBagianDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).hide();
            $('#selectBidangDispo-'+id).prop('disabled', true);
            $('#selectBagianDispo-'+id).prop('disabled', true);
            $('#selectSeksiDispo-'+id).prop('disabled', true);
        }
    }
</script>

@endsection