@extends('layouts.layoutapp')

@section('title')
Sistem Informasi Manajemen Persuratan | Rekap surat masuk
@endsection

@section('sidebar-menu')
<link rel="stylesheet" href="{{URL('css/mail.css')}}">
<li>
    <a href="{{URL('/')}}">
        <i class="fa fa-home"></i> <span>Dashboard</span>
    </a>
</li>

@if ($userLogin->division['name'] == "Umum Kepegawaian")
    <li>
        <a href="umpeg-surat-masuk">
            <i class="fa fa-download"></i>
            <span>Surat Masuk</span>
        </a>
    </li>
@endif

<li class="active">
    <a href="suratku">
        <i class="fa fa-briefcase"></i>
        <span>Suratku</span>
    </a>
</li>

<li>
    <a href="#">
        <i class="fa fa-briefcase"></i>
        <span>Surat Tertunda</span>
    </a>
</li>
@endsection

@section('content')
<section class="content-header" id="list-surat">
<div class="box">
    <div class="box-header">
        <h3>Rekap Surat Masuk</h3>
        <hr>
    </div>
    <div class="box-body">
        <table id="datatable">
          <thead>
            <tr class="active">
              <th>No.</th>
              <th>Tanggal Masuk Surat</th>
              <th>Kode Surat</th>
              <th>Nomor Surat</th>
              <th>Asal Surat</th>
              <th>Tanggal Surat</th>
              <th>Status</th>
              <th>Perihal</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($mails as $index => $mail)
            <tr>
              <td>{{$index+1}}</td>
              <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->created_at))->format('d F Y') }}</td>
              <td>{{$mail->mail_code}}</td>
              <td>{{$mail->sender}}</td>   
              <td>{{$mail->mail_no}}</td>
              <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->mail_date))->format('d F Y') }}</td>
              @if($mail->completion_status == true)
                <td>Selesai</td>
              @else
                <td>Dalam Proses</td>
              @endif
              <td>{{$mail->mail_subject}}</td>                
                        
                @if ($userLogin->role == "Kepala Dinas" || $userLogin->role == "Sekretaris" || $userLogin->role == "Kepala Bidang")
                    @if($mail->completion_status == false)
                        @if ($mail->jobdesc == null)
                            <td><a class="btn btn-info" onclick="addJobdesc ( {{$mail->id}} )">Follow Up</a>
                                <form action="{{URL ('selesaiDispo/'.$mail->id)}}" method="POST" class="form form-horizontal">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="put">
                                    <input type="submit" value="Selesai Disposisi" class="btn btn-danger" onclick="statusDispo()">
                                </form>
                            </td>
                        @else
                            <td>
                                @if ($userLogin->role == "Kepala Dinas" && $mail->status_kadin == true)
                                    <a class="btn btn-success" onclick="dispoSurat ( {{$mail->id}} )">Disposisi Surat</a>
                                @elseif($userLogin->role == "Sekretaris" && $mail->status_sekre == true)
                                    <a class="btn btn-success" onclick="dispoSurat ( {{$mail->id}} )">Disposisi Surat</a>
                                @elseif($userLogin->role == "Kepala Bidang" && $mail->status_department == true)
                                    <a class="btn btn-success" onclick="dispoSurat ( {{$mail->id}} )">Disposisi Surat</a>
                                @endif
                                    <a class="btn btn-info" onclick="detailJob ( {{$mail->id}} )">Detail Follow Up</a>
                                    <a class="btn btn-info" onclick="detailData ( {{$mail->id}} )">Detail Surat</a>
                            </td>                                
                        @endif
                    @else
                        <td>                            
                            <a class="btn btn-info" onclick="detailData ( {{$mail->id}} )">Detail Surat</a>
                            @if ($mail->jobdesc != null)
                                <a class="btn btn-info" onclick="detailJob ( {{$mail->id}} )">Detail Follow Up</a>                            
                            @endif
                        </td>
                    @endif


                @elseif($userLogin->role == "Kepala Sub Bagian" || $userLogin->role == "Kasie")
                    <td>
                        <a class="btn btn-info" onclick="detailData ( {{$mail->id}} )">Detail Surat</a>
                        <a class="btn btn-success" onclick="dispoSurat ( {{$mail->id}} )">Tugaskan Tindak Surat</a>
                    </td>
                @endif
                                 
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
</section>

<!-- disposisi surat -->
@foreach ($mails as $mail)   
    <section class="content-header form-detail" id="dispo-surat-{{$mail->id}}"> 
        <div class="box">
            <div class="box-header">
                <h3>Detail Data Surat Masuk</h3>
                <hr>
            </div>
            <div class="box-body">
    
                <form action="{{URL ('umpeg-surat-masuk/'.$mail->id.'/dispo')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="put">
                    
                        <div class="form-group header-group-0" id="">
                            <label>
                                Pilih Tujuan Surat
                            </label>
                            <select class="form-control" name="tujuan"  id="pilihandispo-{{ $mail->id }}" onchange="gantiPilihan( {{ $mail->id }} )">
                                <option selected disabled>Pilih tujuan surat</option>
                                @if ($userLogin->role == "Kepala Dinas")
                                    <option value="{{$usersekre->id}}">Sekretaris</option>
                                    <option value="bidang">Kepala Bidang</option>
                                    <option value="sub bagian">Kepala Sub Bagian</option>                            
                                    <option value="kasie">Kepala Seksi</option>                                    
                                @elseif($userLogin->role == "Sekretaris")
                                    <option value="bidang">Kepala Bidang</option>
                                    <option value="sub bagian">Kepala Sub Bagian</option>
                                    <option value="kasie">Kepala Seksi</option>
                                @elseif($userLogin->role == "Kepala Bidang")
                                    <option value="kasie">Kepala Seksi</option>
                                @elseif($userLogin->division->name == "Umum Kepegawaian")
                                    @foreach ($staffumpeg as $user )
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach                                    
                                @endif
                            </select>
                        </div>

                    <div class="form-group header-group-0" id="selectStaffDispo-{{ $mail->id }}" disabled style="display:none">
                        <select class="form-control" name="staffDisposisi">
                            @if ($userLogin->division->name == "Umum dan Kepegawaian")
                                @foreach ($staffumpeg as $user )                                    
                                    <option value="{{$user->id}}">{{$user->name}}</option>                                    
                                @endforeach
                            @endif
                            @foreach ($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="form-group header-group-0" id="selectBagianDispo-{{ $mail->id }}" disabled style="display:none">
                        <select class="form-control" name="bagianDisposisi" >
                            @foreach($divisions as $division)
                                <option value="{{$division->id}}">{{$division->name}}</option>
                            @endforeach 
                        </select>
                    </div>
    
                    <div class="form-group header-group-0" id="selectBidangDispo-{{ $mail->id }}" disabled style="display:none">
                        <select class="form-control " name="bidangDisposisi" >
                            @foreach($departments as $department)
                                <option value="{{$department->id}}">{{$department->name}}</option>
                            @endforeach
                        </select>
                    </div>
    
                    <div class="form-group header-group-0" id="selectSeksiDispo-{{ $mail->id }}" disabled style="display:none">
                        <select class="form-control " name="seksiDisposisi" >
                            @foreach($sections as $section)
                                <option value="{{$section->id}}">{{$section->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="dispo-btn">
                        <input type="submit" value="Simpan" class="btn btn-success">
                    </div>
                </form>
    
                <table>
                    <tbody class="table-detail">
                    <tr>
                        <td>Tanggal Masuk Surat</td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->created_at))->format('d F Y') }}</td>
                    </tr>
                    <tr>
                        <td>Kode Surat</td>
                        <td>{{$mail->mail_code}}</td>
                    </tr>
                    <tr>
                        <td>Nomor Surat</td>
                        <td>{{$mail->mail_no}}</td>
                    </tr>
                    <tr>
                        <td>Asal Surat</td>
                        <td>{{$mail->sender}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Surat</td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->mail_date))->format('d F Y') }}</td>
                    </tr>
                    <tr>
                        <td>Kontak</td>
                        <td>{{$mail->contact}}</td>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td>{{$mail->mail_subject}}</td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td>{{$mail->mail_info}}</td>
                    </tr>
                    <tr>
                        <td>Kode Disposisi</td>
                        <td>{{$mail->index->code}}</td>
                    </tr>
                    <tr>
                        <td>Index Disposisi</td>                    
                        <td>{{$mail->index->name}}</td>                    
                    </tr>
                    <tr>
                        <td>Tujuan Disposisi</td>
                        {{-- @if($mail->user_id != null)
                            @foreach($users as $user)
                                @if($user->id == $mail->user_id)
                                    Bidang {{ $user->name }}
                                @endif
                            @endforeach                      
                        @elseif($mail->department_id != null)
                            <td>{{ $mail->department->name }}</td>
                        @elseif($mail->section_id != null)
                            <td>{{ $mail->section->name }}</td>
                        @elseif($mail->division_id != null)
                            <td>{{ $mail->division->name }}</td>
                        @else
                            <td> - </td>
                        @endif --}}
                    </tr>                
                    <tr>
                        <td>Status Disposisi</td>
                        @if($mail->completion_status == true)
                            <td>Selesai</td>
                        @else
                            <td>Dalam Proses</td>
                        @endif
                    </tr>
                    </tbody>                
                </table>
                <div class="padding-btn">
                    <a class="big-btn btn-danger">Kembali</a>   
                </div>
            </div>
        </div>
    </section>    

<!-- jobdesc surat -->  
    <section class="content-header form-jobdesc" id="jobdesc-surat-{{$mail->id}}" style="display:none">
        <div class="box">
            <div class="box-header">
                <h3>Tambah Tindak Lanjut Surat</h3>
                <hr>
            </div>
            <div class="box-body">
                <form action="{{URL ('add-jobdesc/'.$mail->id.'/add') }}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <input type="hidden" name="_method" value="put">
                                        
                    <div class="form-group header-group-0">
                        <label>Deskripsi Tindak Lanjut Surat</label>
                        <textarea class="form-control textarea" name="jobdesc" required></textarea>
                    </div>                    

                    <div class="padding-btn">
                        <input type="submit" value="Simpan" class="big-btn btn-success">
                        <a class="big-btn btn-danger">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </section> 

<!-- detail surat-->    
    <section class="content-header form-detail" id="detail-{{$mail->id}}">
        <div class="box">
            <div class="box-header" style="padding-bottom:0">
                <h3>Detail Data Surat Masuk</h3>
                <hr>
            </div>
            <div class="box-body">            
                <table>
                    <tbody class="table-detail">
                    @if ($mail->jobdesc)                    
                        <tr>
                            <td colspan="2">Deskripsi Tindak Lanjut Surat</td>
                        </tr>                    
                        <tr>
                        <td colspan="2" style="font-weight: normal;text-align:justify">
                                <p>{!!$mail->jobdesc!!}</p>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>Tanggal Masuk Surat</td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->created_at))->format('d F Y') }}</td>
                    </tr>
                    <tr>
                        <td>Kode Surat</td>
                        <td>{{$mail->mail_code}}</td>
                    </tr>
                    <tr>
                        <td>Nomor Surat</td>
                        <td>{{$mail->mail_no}}</td>
                    </tr>
                    <tr>
                        <td>Asal Surat</td>
                        <td>{{$mail->sender}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Surat</td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($mail->mail_date))->format('d F Y') }}</td>
                    </tr>
                    <tr>
                        <td>Kontak</td>
                        <td>{{$mail->contact}}</td>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td>{{$mail->mail_subject}}</td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td>{{$mail->mail_info}}</td>
                    </tr>
                    <tr>
                        <td>Kode Disposisi</td>
                        <td>{{$mail->index->code}}</td>
                    </tr>
                    <tr>
                        <td>Index Disposisi</td>                    
                        <td>{{$mail->index->name}}</td>                    
                    </tr>
                    <tr>
                        <td>Tujuan Disposisi</td>                    
                        @if($mail->user_id != null)
                            @foreach($users as $user)
                                @if($user->id == $mail->user_id)
                                    <td>{{ $user->role }}</td>
                                @endif
                            @endforeach                      
                        @elseif($mail->department_id != null)
                            <td>{{ $mail->department->name }}</td>
                        @elseif($mail->section_id != null)
                            <td>{{ $mail->section->name }}</td>
                        @elseif($mail->division_id != null)
                            <td>{{ $mail->division->name }}</td>
                        @else
                            <td> - </td>
                        @endif
                    </tr>
                    </tbody>                
                </table>
                <div class="padding-btn">
                    <a class="big-btn btn-danger">Kembali</a>   
                </div>
            </div>
        </div>
    </section> 

<!-- detail Follow Up-->
    <section class="content-header form-detail" id="jobdesc-{{$mail->id}}">
        <div class="box">
            <div class="box-header" style="padding-bottom:0">
                <h3>Detail Follow Up Surat</h3>
                <hr>
            </div>
            <div class="box-body">
                <div>
                    <table>
                        <tr>
                            <td>
                                {!!$mail->jobdesc!!}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="padding-btn">
                    <a class="big-btn btn-danger">Kembali</a>   
                </div>
            </div>
        </div>
    </section>
@endforeach

<script>
    $(".textarea").wysihtml5({
        toolbar:{
            "font-styles": false, // Font styling, e.g. h1, h2, etc.
            "emphasis": false, // Italics, bold, etc.
            "lists": false, // (Un)ordered lists, e.g. Bullets, Numbers.
            "html": false, // Button which allows you to edit the generated HTML.
            "link": false, // Button to insert a link.
            "image": false, // Button to insert an image.
            "color": false, // Button to change color of font
            "blockquote": false, // Blockquote
            
        }
    });

    $('#datatable').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
    });

    function dispoSurat(id){
        $("#dispo-surat-"+id).show();
        $("#list-surat").hide();        
    }

    function detailData(id){
        $(".form-detail#detail-"+id).show();
        $("#list-surat").hide();
    }

    $('.btn-danger').click(function(){
        $("#list-surat").show();
        $(".form-detail").hide();
        $("#btn-dispo-"+id).show();
    })

    function addJobdesc(id){
        $('.form-jobdesc#jobdesc-surat-'+id).show();
        $("#list-surat").hide();
    }

    $('.btn-danger').click(function(){
        $("#list-surat").show();
        $(".form-jobdesc").hide();
    })

    function detailJob(id){
        $('#jobdesc-'+id).show();
        $('#list-surat').hide();
    }
    $('.btn-danger').click(function(){
        $("#list-surat").show();
        $('#jobdesc-'+id).hide();
    })

    function gantiPilihan(id){
        var tujuan = $('#pilihandispo-'+id).val();
        console.log('tujuan ke = '+tujuan+' dengan id = '+id)

        if(tujuan == 'bidang'){
            $('#selectBidangDispo-'+id).show();
            $('#selectBagianDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).hide();
            $('#selectBidangDispo-'+id).prop('disabled', false);
        }else if(tujuan == 'sub bagian'){
            $('#selectBagianDispo-'+id).show();
            $('#selectBidangDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).hide();
            $('#selectBagianDispo-'+id).prop('disabled', false);
        }else if(tujuan == 'kasie'){
            $('#selectSeksiDispo-'+id).show();
            $('#selectBagianDispo-'+id).hide();
            $('#selectBidangDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).prop('disabled', false);
        }else{
            $('#selectBidangDispo-'+id).hide();
            $('#selectBagianDispo-'+id).hide();
            $('#selectSeksiDispo-'+id).hide();
            $('#selectBidangDispo-'+id).prop('disabled', true);
            $('#selectBagianDispo-'+id).prop('disabled', true);
            $('#selectSeksiDispo-'+id).prop('disabled', true);
        }
    }
</script>

@endsection