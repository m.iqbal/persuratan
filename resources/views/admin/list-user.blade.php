@extends('layouts.layoutapp')

@section('title')
Sistem Informasi Manajemen Persuratan | User
@endsection

@section('sidebar-menu')
<link rel="stylesheet" href="{{URL('css/admin/style.css')}}">  
    <li>
        <a href="{{URL('/')}}">
        <i class="fa fa-book"></i> <span>Dashboard</span>
        </a>
    </li>
    <li class="active">
        <a href="{{URL('user')}}">
        <i class="fa fa-users"></i>
        <span>Kelola User</span>
        </a>
    </li>

    <li>
        <a href="{{URL('department')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Bidang</span>
        </a>
    </li>

    <li>
        <a href="{{URL('section')}}">
            <i class="fa fa-briefcase"></i>
            <span>Kelola Seksi</span>
        </a>
    </li>

    <li>
        <a href="{{URL('division')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Sub Bagian</span>
        </a>
    </li>
      
    <li>
        <a href="{{URL('index')}}">
        <i class="fa fa-search"></i>
        <span>Kelola Index</span>
        </a>
    </li>
@endsection

@section('content')
<section class="content-header" id="list-user">
<div class="box">
    <div class="box-header">
        <h3>List User</h3>
        <hr>
        <a class="btn btn-success" id="button">
        <i class="fa fa-plus-square"></i>
        Tambah Data</a>
    </div>
    <div class="box-body">
        <table id="datatable">
            <thead>
                <tr class="active">
                    <th id="nomor">No.</th>
                    <th>Nama</th>
                    <th>Username</th>              
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($users as $index => $user)
                <tr>
                <td>{{$index+1}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->username}}</td>

                @if($user->role == "Kepala Bidang")
                    <td>{{$user->role}} {{$user->department->name}}</td>
                @elseif($user->role == "Kepala Sub Bagian")
                    <td>{{$user->role}} {{$user->division->name}}</td>
                @elseif($user->role == "Kasie")
                    <td>{{$user->role}} {{$user->section->name}}</td>
                @elseif($user->role == "Staff")
                    @if($user->section_id != null)
                        <td>{{$user->role}} {{$user->section->name}}</td>
                    @elseif($user->division_id != null)
                        <td>{{$user->role}} {{$user->division->name}}</td>
                    @endif
                @else
                    <td>{{$user->role}}</td>
                @endif

                <td class="btn-aksi">
                    <a class="btn btn-warning" onclick="editUser( {{$user->id}} )">Edit</a>
                    <a class="btn btn-danger" onclick="deleteUser( {{$user->id}} )">Hapus</a>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</section>

<section class="content" id="form-user">
    <div class="box">
        <div class="box-header">
            <h3>Tambah User</h3>
            <hr>
        </div>
        <div class="box-body">
            <form action="{{ URL('user')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
                <div class="form-group header-group-0" id="">
                    <label >
                        Nama
                    </label>
                    <input type="text" required class="form-control" name="nama" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Username
                    </label>
                    <input type="text" required class="form-control" name="username" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Password
                    </label>
                    <input type="text" required class="form-control" name="password" id="">                    
                </div>
                
                <div class="form-group header-group-0" id="">
                    <label >
                        Role
                    </label>
                    <select name="role" required class="form-control" id="selectrole">
                        <option value="Admin">Admin</option>
                        <option value="Kepala Dinas">Kepala Dinas</option>
                        <option value="Sekretaris">Sekretaris</option>
                        <option value="Kepala Bidang">Kepala Bidang</option>
                        <option value="Kepala Sub Bagian">Kepala Sub Bagian</option>
                        <option value="Kasie">Kasie</option>
                        <option value="Staff">Staff</option>
                        <option value="Operator Surat">Operator Surat</option>
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectBagian" style="display: none">
                    <label>
                        Sub Bagian
                    </label>
                    <select name="rolebagian" required class="form-control" disabled>
                        @foreach($divisions as $division)
                            <option value="{{$division->id}}">{{$division->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectBidang" style="display: none">
                    <label >
                        Bidang
                    </label>
                    <select name="rolebidang" required class="form-control" disabled>
                        @foreach($departments as $department)
                            <option value="{{$department->id}}">{{$department->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectSeksi" style="display: none">
                    <label >
                        Seksi
                    </label>
                    <select name="roleseksi" required class="form-control" disabled>
                        @foreach($sections as $section)
                            <option value="{{$section->id}}">{{$section->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectStaff" style="display: none">
                    <label >
                        Seksi / Sub Bagian
                    </label>
                    <select name="rolestaff" required class="form-control" id="selectstaff" disabled>
                        <option value="" selected disabled>Seksi / Sub Bagian</option>
                        <option value="seksi">Seksi</option>
                        <option value="subag">Sub Bagian</option>
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectSeksi1" style="display: none">
                    <label >
                        Staff Seksi
                    </label>
                    <select name="rolestaffseksi" required class="form-control" disabled>
                        @foreach($sections as $section)
                            <option value="{{$section->id}}">{{$section->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectSubag1" style="display: none">
                    <label >
                        Staff Sub Bagian
                    </label>
                    <select name="rolestaffsubag" required class="form-control" disabled>
                        @foreach($divisions as $division)
                                <option value="{{$division->id}}">{{$division->name}}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>

@foreach($users as $user)
<section class="content form-edit" id="edit-{{$user->id}}">
    <div class="box">
        <div class="box-header">
            <h3>Edit User</h3>            
        </div>
        <div class="box-body">
            <form action="{{ URL('user/'.$user->id.'/edit')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
                <div class="form-group header-group-0">
                    <label >
                        Nama
                    </label>
                    <input type="text" value="{{$user->name}}" required class="form-control" name="nama" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Username
                    </label>
                    <input type="text" value="{{$user->username}}" required class="form-control" name="username" id="">                    
                </div>

                <div class="form-group header-group-0" id="">
                    <label >
                        Password
                    </label>
                    <input type="text" value="{{$user->password}}" required class="form-control" name="password" id="">                    
                </div>
                
                <div class="form-group header-group-0" id="">
                    <label >
                        Role
                    </label>
                    <select name="role" required class="form-control" onchange="showPilihan( {{ $user->id }} )" id="selectroleedit-{{ $user->id }}">
                        <option value="" selected disabled>{{$user->role}}</option>
                        <option value="Admin">Admin</option>
                        <option value="Kepala Dinas">Kepala Dinas</option>
                        <option value="Sekretaris">Sekretaris</option>
                        <option value="Kepala Bidang">Kepala Bidang</option>
                        <option value="Kepala Sub Bagian">Kepala Sub Bagian</option>
                        <option value="Kasie">Kasie</option>
                        <option value="Staff">Staff</option>
                        <option value="Operator Surat">Operator Surat</option>
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectBagianEdit-{{ $user->id }}" style="display: none">
                    <label >
                        Sub Bagian
                    </label>
                    <select name="rolebagian" required class="form-control" disabled>
                        @foreach($divisions as $division)
                            <option value="{{$division->id}}">{{$division->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectBidangEdit-{{ $user->id }}" style="display: none">
                    <label >
                        Bidang
                    </label>
                    <select name="rolebidang" required class="form-control" disabled>
                        @foreach($departments as $department)
                            <option value="{{$department->id}}">{{$department->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectSeksiEdit-{{ $user->id }}" style="display: none">
                    <label >
                        Seksi
                    </label>
                    <select name="roleseksi" required class="form-control" disabled>
                        @foreach($sections as $section)
                            <option value="{{$section->id}}">{{$section->name}}</option>
                        @endforeach
                    </select>
                </div>
    
                <div class="form-group header-group-0" id="selectStaffEdit-{{ $user->id }}" style="display: none">
                    <label >
                        Seksi / Sub Bagian
                    </label>
                    <select name="rolestaff" required class="form-control" onchange="showPilihanStaff( {{ $user->id }} )" id="selectstaffedit-{{$user->id}}" disabled>
                        <option value="" selected disabled>Seksi / Sub Bagian</option>
                        <option value="seksi">Seksi</option>
                        <option value="subag">Sub Bagian</option>
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectSeksi1Edit-{{$user->id}}" style="display: none">
                    <label >
                        Staff Seksi
                    </label>
                    <select name="rolestaffseksi" required class="form-control" disabled>
                        @foreach($sections as $section)
                            <option value="{{$section->id}}">{{$section->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group header-group-0" id="selectSubag1Edit-{{$user->id}}" style="display: none">
                    <label >
                        Staff Sub Bagian
                    </label>
                    <select name="rolestaffsubag" required class="form-control" disabled>
                        @foreach($divisions as $division)
                                <option value="{{$division->id}}">{{$division->name}}</option>
                        @endforeach
                    </select>
                </div>

                <input type="submit" value="Simpan" class="big-btn btn-success">
                <a class="big-btn btn-danger">Batal</a>
            </form>
        </div>
    </div>
</section>

<form action="{{ URL('user/'.$user->id.'/delete') }}" class="form-hapus" id="hapus-{{ $user->id }}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="delete">
</form>
@endforeach

<script>
$('#datatable').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
})

$("#button").click(function(){
  $("#list-user").hide();
  $("#form-user").show();
});

$(".btn-danger").click(function(){
  $("#list-user").show();
  $("#form-user").hide();
});

function editUser(id){
  $(".form-edit#edit-"+id).show();
  $("#list-user").hide();
}

$(".btn-danger").click(function(){
  $(".form-edit").hide();
  $("#list-user").show();
});

function deleteUser(id){
    $(".form-hapus#hapus-"+id).submit();
}

$('#selectrole').bind('change', function(event){
    var role = $('#selectrole').val();
    console.log(role);
    if(role == 'Kepala Sub Bagian'){
        $('#selectBagian').show();
        $('#selectBidang').hide();
        $('#selectSeksi').hide();
        $('#selectStaff').hide();
        $('#selectSeksi1').hide();
        $('#selectSubag1').hide();
        $('#selectBagian select').prop('disabled', false);
    }else if(role == 'Kepala Bidang'){
        $('#selectBidang').show();
        $('#selectBagian').hide();
        $('#selectSeksi').hide();
        $('#selectStaff').hide();
        $('#selectSeksi1').hide();
        $('#selectSubag1').hide();
        $('#selectBidang select').prop('disabled', false);
    }else if(role == 'Kasie'){
        $('#selectSeksi').show();
        $('#selectBagian').hide();
        $('#selectBidang').hide();
        $('#selectStaff').hide();
        $('#selectSeksi1').hide();
        $('#selectSubag1').hide();
        $('#selectSeksi select').prop('disabled', false);
    }else if(role == 'Staff'){
        $('#selectStaff').show();
        $('#selectSeksi').hide();
        $('#selectBagian').hide();
        $('#selectBidang').hide();
        $('#selectStaff select').prop('disabled', false);
            
            $('#selectStaff').bind('change', function(event){
            var rolestaff = $('#selectstaff').val();
                console.log(rolestaff);
                if(rolestaff == 'seksi'){
                    $('#selectSeksi1').show();
                    $('#selectSubag1').hide();
                    $('#selectSeksi1 select').prop('disabled', false);
                }else if(rolestaff == 'subag'){
                    $('#selectSubag1').show();
                    $('#selectSeksi1').hide();
                    $('#selectSubag1 select').prop('disabled', false);
                }
            });
    }else{
        $('#selectBidang').hide();
        $('#selectBagian').hide();
        $('#selectSeksi').hide();
        $('#selectStaff').hide();
        $('#selectSeksi1').hide();
        $('#selectSubag1').hide();
        $('#selectBidang select').prop('disabled', true);
        $('#selectBagian select').prop('disabled', true);
        $('#selectSeksi select').prop('disabled', true);
        $('#selectStaff select').prop('disabled', true);
        $('#selectSeksi1 select').prop('disabled', true);
        $('#selectSubag1 select').prop('disabled', true);
    }
});

function showPilihan(id){
    var role = $('#selectroleedit-'+id).val();    
    
    if(role == 'Kepala Sub Bagian'){
        $('#selectBagianEdit-'+id).show();
        $('#selectBidangEdit-'+id).hide();
        $('#selectSeksiEdit-'+id).hide();
        $('#selectStaffEdit-'+id).hide();
        $('#selectSubag1Edit-'+id).hide();
        $('#selectSeksi1Edit-'+id).hide();
        $('#selectBagianEdit-'+id+' select').prop('disabled', false);
    }else if(role == 'Kepala Bidang'){
        $('#selectBidangEdit-'+id).show();
        $('#selectBagianEdit-'+id).hide();
        $('#selectSeksiEdit-'+id).hide();
        $('#selectStaffEdit-'+id).hide();
        $('#selectSubag1Edit-'+id).hide();
        $('#selectSeksi1Edit-'+id).hide();
        $('#selectBidangEdit-'+id+' select').prop('disabled', false);
    }else if(role == 'Kasie'){
        $('#selectSeksiEdit-'+id).show();
        $('#selectBagianEdit-'+id).hide();
        $('#selectBidangEdit-'+id).hide();
        $('#selectStaffEdit-'+id).hide();
        $('#selectSeksiEdit-'+id+' select').prop('disabled', false);
    }else if(role == 'Staff'){
        $('#selectStaffEdit-'+id).show();
        $('#selectSeksiEdit-'+id).hide();
        $('#selectBagianEdit-'+id).hide();
        $('#selectBidangEdit-'+id).hide();
        $('#selectStaffEdit-'+id+' select').prop('disabled', false);             
    }else{
        $('#selectBidangEdit-'+id).hide();
        $('#selectBagianEdit-'+id).hide();
        $('#selectSeksiEdit-'+id).hide();
        $('#selectStaffEdit-'+id).hide();
        $('#selectSubag1Edit-'+id).hide();
        $('#selectSeksi1Edit-'+id).hide();
        $('#selectBidangEdit-'+id+' select').prop('disabled', true);
        $('#selectBagianEdit-'+id+' select').prop('disabled', true);
        $('#selectSeksiEdit-'+id+' select').prop('disabled', true);
        $('#selectStaffEdit-'+id+' select').prop('disabled', true);
        $('#selectSubag1Edit-'+id+' select').prop('disabled', true);
        $('#selectSeksi1Edit-'+id+' select').prop('disabled', true);
    }
}

function showPilihanStaff(id){
    var rolestaff = $('#selectstaffedit-'+id).val();
    console.log(rolestaff);
    if(rolestaff == 'seksi'){
        $('#selectSeksi1Edit-'+id).show();
        $('#selectSubag1Edit-'+id).hide();
        $('#selectSeksi1Edit-'+id+' select').prop('disabled', false);
    }else if(rolestaff == 'subag'){
        $('#selectSubag1Edit-'+id).show();
        $('#selectSeksi1Edit-'+id).hide();
        $('#selectSubag1Edit-'+id+' select').prop('disabled', false);
    }
}
</script>
@endsection