@extends('layouts.layoutapp')

@section('title')
Sistem Informasi Manajemen Persuratan | Kasie
@endsection

@section('sidebar-menu')
<link rel="stylesheet" href="{{URL('css/admin/style.css')}}">
    <li>
        <a href="{{URL('/')}}">
            <i class="fa fa-book"></i> <span>Dashboard</span>
        </a>
    </li>

    <li>
        <a href="{{URL('user')}}">
            <i class="fa fa-users"></i>
            <span>Kelola User</span>
        </a>
    </li>

    <li>
        <a href="{{URL('department')}}">
            <i class="fa fa-briefcase"></i>
            <span>Kelola Bidang</span>
        </a>
    </li>

    <li class="active">
        <a href="{{URL('section')}}">
            <i class="fa fa-briefcase"></i>
            <span>Kelola Seksi</span>
        </a>
    </li>

    <li>
        <a href="{{URL('division')}}">
            <i class="fa fa-briefcase"></i>
            <span>Kelola Sub Bagian</span>
        </a>
    </li>

    <li>
        <a href="{{URL('index')}}">
            <i class="fa fa-search"></i>
            <span>Kelola Index</span>
        </a>
    </li>
@endsection

@section('content')
<section class="content-header" id="list-kasie">
<div class="box">
    <div class="box-header">
        <h3>List Kasie</h3>
        <hr>
        <a class="btn btn-success" id="button">
        <i class="fa fa-plus-square"></i>
        Tambah Data</a>
    </div>

    <div class="box-body">
        <table id="datatable">
          <thead>
            <tr class="active">
              <th id="nomor">No</th>              
              <th>Nama Kasie</th>
              <th>Keterangan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($sections as $index => $section)
            <tr>
                <td>{{$index+1}}</td>                
                <td>{{$section->name}}</td>
                <td>{{$section->information}}</td>
                <td class="btn-aksi">
                    <a class="btn btn-warning" onclick="editKasie( {{$section->id}} )">Edit</a>
                    <a class="btn btn-danger" onclick="deleteKasie( {{$section->id}} )">Hapus</a>
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
</section>

<section class="content" id="form-kasie">
    <div class="box">
        <div class="box-header">
            <h3>Tambah Kasie</h3>       
            <hr>
        </div>
        <div class="box-body">
            <form action="{{URL('section')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}                

                <div class="form-group header-group-0" >
                    <label >
                        Nama Kasie
                    </label>
                    <input type="text" required class="form-control" name="nama_seksi">                    
                </div>

                <div class="form-group header-group-0">
                    <label >
                        Keterangan
                    </label>
                    <textarea class="form-control" name="keterangan"></textarea>                    
                </div>

                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>

@foreach($sections as $section)
<section class="content form-edit" id="edit-{{$section->id}}">
    <div class="box">
        <div class="box-header">
            <h3>Edit Kasie</h3>
            <hr>            
        </div>
        <div class="box-body">
        <form action="{{URL ('section/'.$section->id.'/edit') }}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
        
                <div class="form-group header-group-0" id="form-group-nomorsurat">
                    <label >
                        Nama kasie
                    </label>
                    <input type="text" value="{{$section->nama}}" required class="form-control" name="nama_seksi">
                </div>

                <div class="form-group header-group-0" id="form-group-kontak">
                    <label >
                        Keterangan
                    </label>
                    <textarea required class="form-control" name="keterangan">{{$section->keterangan}}</textarea>
                </div>

                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>

<form action="{{URL ('section/'.$section->id.'/delete') }}" class="form-hapus" id="hapus-{{$section->id}}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="delete">
</form>

@endforeach

<script>
$('#datatable').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
})

$("#button").click(function(){
  $("#list-kasie").hide();
  $("#form-kasie").show();
});

$(".btn-danger").click(function(){
  $("#list-kasie").show();
  $("#form-kasie").hide();
});

function editKasie(id){
  $(".form-edit#edit-"+id).show();
  $("#list-kasie").hide();
};

$(".btn-danger").click(function(){
  $(".form-edit").hide();
  $("#list-kasie").show();
});

function deleteKasie(id){
    $(".form-hapus#hapus-"+id).submit();
}

</script>

@endsection