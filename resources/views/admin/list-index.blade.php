@extends('layouts.layoutapp')

@section('title')
Sistem Informasi Manajemen Persuratan | Index
@endsection

@section('sidebar-menu')
<link rel="stylesheet" href="{{URL('css/admin/style.css')}}">
<li>
    <a href="{{URL('/')}}">
        <i class="fa fa-book"></i> <span>Dashboard</span>
    </a>
</li>

<li>
    <a href="{{URL('user')}}">
        <i class="fa fa-users"></i>
        <span>Kelola User</span>
    </a>
</li>

<li>
    <a href="{{URL('department')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Bidang</span>
    </a>
</li>

<li>
    <a href="{{URL('section')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Seksi</span>
    </a>
</li>
    
<li>
    <a href="{{URL('division')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Sub Bagian</span>
    </a>
</li>

<li class="active">
    <a href="{{URL('index')}}">
        <i class="fa fa-search"></i>
        <span>Kelola Index</span>
    </a>
</li>
@endsection

@section('content')
<section class="content-header" id="list-index">
<div class="box">
    <div class="box-header">
        <h3>List Index</h3>
        <hr>
        <a class="btn btn-success" id="button">
        <i class="fa fa-plus-square"></i>  
        Tambah Data</a>
    </div>
    <div class="box-body">
        <table id="datatable">
        <thead>
            <tr class="active">
            <th id="nomor">No.</th>
            <th>Kode</th>
            <th>Index Disposisi</th>
            <th>Keterangan</th>
            <th>Action</th>
            </tr>
        </thead>
            <tbody>
                @foreach($indices as $index => $dIndex)
                    <tr>
                    <td>{{$index+1}}</td>
                    <td>{{$dIndex->code}}</td>
                    <td>{{$dIndex->name}}</td>
                    <td>{{$dIndex->information}}</td>
                    <td class="btn-aksi">
                        <a class="btn btn-warning" onclick="editIndex ( {{$dIndex->id}} )">Edit</a>
                        <a class="btn btn-danger" onclick="deleteIndex ( {{$dIndex->id}} )">Hapus</a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</section>

<section class="content" id="form-index">
    <div class="box">
        <div class="box-header">
            <h3>Tambah Index</h3>
            <hr>
        </div>
        <div class="box-body">
            <form action="{{URL('index')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
                <div class="form-group header-group-0">
                    <label >
                        Kode
                    </label>
                    <input type="text" required class="form-control" name="kode">                    
                </div>

                <div class="form-group header-group-0">
                    <label >
                        Nama Index
                    </label>
                    <input type="text" required class="form-control" name="nama_index" >                    
                </div>

                <div class="form-group header-group-0">
                    <label >
                        Keterangan
                    </label>
                    <textarea title="Keterangan" required maxlength="255" class="form-control" name="keterangan"></textarea>                    
                </div>

                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>

@foreach($indices as $dIndex)
<section class="content form-edit" id="edit-{{$dIndex->id}}">
    <div class="box">
        <div class="box-header">
            <h3>Edit Index</h3>
        </div>

        <div class="box-body">
            <form action="{{URL('index/'.$dIndex->id.'/edit')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
                <div class="form-group header-group-0">
                    <label >
                        Kode
                    </label>
                    <input type="text" value="{{$dIndex->code}}" required class="form-control" name="kode">                    
                </div>

                <div class="form-group header-group-0">
                    <label >
                       Nama Index
                    </label>
                    <input type="text" value="{{$dIndex->nama}}" required class="form-control" name="nama_index">                    
                </div>

                <div class="form-group header-group-0">
                    <label >
                        Keterangan
                    </label>
                    <textarea required class="form-control" name="keterangan">{{$dIndex->keterangan}}</textarea>                    
                </div>

                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>

<form action="{{ URL('index/'.$dIndex->id.'/delete') }}" class="form-hapus" id="hapus-{{ $dIndex->id }}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="delete">
</form>

@endforeach

<script>
$('#datatable').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
})

$("#button").click(function(){
$("#list-index").hide();
$("#form-index").show();
});

$(".btn-danger").click(function(){
$("#list-index").show();
$("#form-index").hide();
});

function editIndex(id){
$(".form-edit#edit-"+id).show();
$("#list-index").hide();
}

$(".btn-danger").click(function(){
$(".form-edit").hide();
$("#list-index").show();
});

function deleteIndex(id){
    // event.preventDefault();
    $(".form-hapus#hapus-"+id).submit();
}

</script>
@endsection