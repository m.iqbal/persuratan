@extends('layouts.layoutapp')

@section('title')
Sistem Informasi Manajemen Persuratan | Sub Bagian
@endsection

@section('sidebar-menu')
<link rel="stylesheet" href="{{URL('css/admin/style.css')}}">
<li>
    <a href="{{URL('/')}}">
        <i class="fa fa-book"></i> 
        <span>Dashboard</span>
    </a>
</li>

<li>
    <a href="{{URL('user')}}">
        <i class="fa fa-users"></i>
        <span>Kelola User</span>
    </a>
</li>

<li>
    <a href="{{URL('department')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Bidang</span>
    </a>
</li>

<li>
    <a href="{{URL('section')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Seksi</span>
    </a>
</li>

<li class="active">
    <a href="{{URL('division')}}">
        <i class="fa fa-briefcase"></i>
        <span>Kelola Sub Bagian</span>
    </a>
</li>

<li>
    <a href="index">
        <i class="fa fa-search"></i>
        <span>Kelola Index</span>
    </a>
</li>
@endsection

@section('content')
<section class="content-header" id="list-bagian">
<div class="box">
    <div class="box-header">
        <h3>List Sub Bagian</h3>
        <hr>
        <a class="btn btn-success" id="button">
        <i class="fa fa-plus-square"></i>
        Tambah Data</a>
    </div>
    <div class="box-body">
        <table id="datatable">
          <thead>
            <tr class="active">
              <th id="nomor">No</th>              
              <th>Nama Sub Bagian</th>          
              <th>Keterangan</th>          
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($divisions as $index => $division)
            <tr>
                <td>{{$index+1}}</td>                
                <td>{{$division->name}}</td>
                <td>{{$division->information}}</td>
                <td class="btn-aksi">
                    <a class="btn btn-warning" onclick="editBagian( {{$division->id}} )">Edit</a>
                    <a class="btn btn-danger" onclick="deleteBagian( {{$division->id}} )">Hapus</a>
                </td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
</section>

<section class="content" id="form-bagian">
    <div class="box">
        <div class="box-header">
            <h3>Tambah Sub Bagian</h3>
            <hr>
        </div>
        <div class="box-body">
            <form action="{{URL('division')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field(   )}}                

                <div class="form-group header-group-0">
                    <label >
                        Nama Bagian
                    </label>
                    <input type="text" required class="form-control" name="nama_subag">                    
                </div>
                
                <div class="form-group header-group-0">
                    <label >
                        Keterangan
                    </label>
                    <textarea class="form-control" name="keterangan"></textarea>                    
                </div>
    
                <div class="padding-btn">
                    <input type="submit" value="Simpan" class="big-btn btn-success">
                    <a class="big-btn btn-danger">Batal</a>
                </div>
            </form>
        </div>
    </div>
</section>
@foreach($divisions as $division)
<section class="content form-edit" id="edit-{{$division->id}}" >
    <div class="box">
        <div class="box-header">
            <h3>Edit Bagian</h3>
            <hr>
        </div>
        <div class="box-body">
            <form action="{{URL('division/'.$division->id.'/edit')}}" method="POST" id="form" class="form form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
        
                <div class="form-group header-group-0">
                    <label >
                        Nama Bagian
                    </label>
                    <input type="text" value="{{$division->name}}" required class="form-control" name="nama_subag" id="">                    
                </div> 

                <div class="form-group header-group-0">
                <label >
                    Keterangan
                </label>
                <textarea class="form-control" name="keterangan">{{$division->information}}</textarea>                    
            </div>

            <div class="padding-btn">
                <input type="submit" value="Simpan" class="big-btn btn-success">
                <a class="big-btn btn-danger">Batal</a>
            </div>
            </form>
        </div>
    </div>
</section>

<form action="{{ URL('division/'.$division->id.'/delete')}}" class="form-hapus" id="hapus-{{$division->id}}" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="delete">
</form>

@endforeach

<script>
$('#datatable').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
})

$("#button").click(function(){
  $("#list-bagian").hide();
  $("#form-bagian").show();
});

$(".btn-danger").click(function(){
  $("#list-bagian").show();
  $("#form-bagian").hide();
});

function editBagian(id){
  $(".form-edit#edit-"+id).show();
  $("#list-bagian").hide();
};

$(".btn-danger").click(function(){
  $(".form-edit").hide();
  $("#list-bagian").show();
});

function deleteBagian(id){
    $(".form-hapus#hapus-"+id).submit();
}

</script>

@endsection