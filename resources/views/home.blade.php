@extends('layouts.layoutapp')
@section('title')
AdminLTE 2 | Dashboard
@endsection

@section('sidebar-menu')

    <li class="active">
        <a href="{{URL('/')}}">
        <i class="fa fa-home"></i> <span>Dashboard</span>
        </a>
    </li>
    @if($userLogin->role == 'Admin')

    <li>
        <a href="{{URL('user')}}">
            <i class="fa fa-users"></i>
            <span>Kelola User</span>
        </a>
    </li>

    <li>
        <a href="{{URL('department')}}">
            <i class="fa fa-briefcase"></i>
            <span>Kelola Bidang</span>
        </a>
    </li>

    <li>
        <a href="{{URL('section')}}">
            <i class="fa fa-briefcase"></i>
            <span>Kelola Seksi</span>
        </a>
    </li>

    <li>
        <a href="{{URL('division')}}">
            <i class="fa fa-briefcase"></i>
            <span>Kelola Sub Bagian</span>
        </a>
    </li>

    <li>
        <a href="{{URL('index')}}">
            <i class="fa fa-search"></i>
            <span>Kelola Index</span>
        </a>
    </li>
    @endif

    @if($userLogin->role == 'Kepala Dinas' || $userLogin->role == 'Sekretaris' || $userLogin->role == 'Kepala Sub Bagian' || $userLogin->role == 'Kepala Bidang' )
        
        @if ($userLogin->role == 'Kepala Sub Bagian')
        @if($userLogin->division->name == 'Umum Kepegawaian')
        <li>
            <a href="{{URL('umpeg-surat-masuk')}}">
                    <i class="fa fa-download"></i>
                    <span>Surat Masuk</span>
                </a>
            </li>
            @endif
        @endif    

        <li>
            <a href="{{URL('suratku')}}">
            <i class="fa fa-briefcase"></i>
            <span>Suratku</span>
            </a>
        </li>     
            
        <li>
            <a href="{{URL('surat-tertunda')}}">
            <i class="fa fa-envelope"></i>
            <span>Surat Tertunda</span>
            </a>
        </li>
    @endif
    {{--
        @if($userLogin->role == 'Staff')

        <li>
            <a href="{{URL('buat-surat')}}">
            <i class="fa fa-plus-square"></i>
            <span>Buat Surat</span>
            </a>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-cart-plus"></i>
                <span>Keranjang Surat</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu" style="display:none;">
                <li>
                    <a href="{{URL('keranjang-surat-undangan')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Surat Undangan</span>
                    </a>
                </li>

                <li>
                    <a href="{{URL('keranjang-surat-edaran')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Surat Edaran</span>
                    </a>
                </li>

                <li>
                    <a href="{{URL('keranjang-nota-dinas')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Nota Dinas</span>
                    </a>
                </li>

                <li>
                    <a href="{{URL('keranjang-surat-keterangan')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Surat Keterangan</span>
                    </a>
                </li>

                <li>
                    <a href="{{URL('keranjang-surat-pengantar')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Surat Pengantar</span>
                    </a>
                </li>

                <li>
                    <a href="{{URL('keranjang-surat-rekomendasi')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Surat Rekomendasi</span>
                    </a>
                </li>

                <li>
                    <a href="{{URL('keranjang-surat-tugas')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Surat Tugas</span>
                    </a>
                </li>

                <li>
                    <a href="{{URL('keranjang-surat-lainnya')}}">
                    <i class="fa fa-circle-o"></i>
                    <span>Surat Lainnya</span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="{{URL('#')}}">
                <i class="fa fa-tasks"></i>
                <span>Tugas</span>
            </a>
        </li>
        @endif--}}

        @if($userLogin->role == 'Operator Surat')      
        <li>
            <a href="{{URL('surat-masuk')}}">
            <i class="fa fa-download"></i>
            <span>Surat Masuk</span>
            </a>
        </li>

        <li>
            <a href="#">
            <i class="fa fa-upload"></i>
            <span>Surat Keluar</span>
            </a>
        </li>
    @endif

@endsection

@section('content')
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
</section>

  <section class="content">    
    <div class="row">
      <div class="col-lg-3 col-xs-6">        
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>150</h3>
            <p>Surat Masuk</p>
          </div>
          <div class="icon">
            <i class="fa fa-envelope-open"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      
      <div class="col-lg-3 col-xs-6">
        
        <div class="small-box bg-green">
          <div class="inner">
            <h3>53</h3>

            <p>Surat Keluar</p>
          </div>
          <div class="icon">
            <i class="fa fa-envelope"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        
        <div class="nav-tabs-custom">
          
          <ul class="nav nav-tabs pull-right">
            <li class="pull-left header"><i class="fa fa-inbox"></i>Grafik Surat Masuk dan Surat Keluar</li>
          </ul>
          <div class="tab-content no-padding">
            
            <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 400px;"></div>
          </div>
        </div>
      </section>
    </div>

  </section>
@endsection