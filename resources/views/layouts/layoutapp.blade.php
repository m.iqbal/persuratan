<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    @yield('title')
  </title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <link rel="stylesheet" href="{{URL('template/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  
  <link rel="stylesheet" href="{{URL('template/bower_components/font-awesome/css/font-awesome.min.css')}}">
  
  <link rel="stylesheet" href="{{URL('template/bower_components/Ionicons/css/ionicons.min.css')}}">
  
  <link rel="stylesheet" href="{{URL('template/dist/css/AdminLTE.min.css')}}">

  <link rel="stylesheet" href="{{URL('css/template.css')}}">
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{URL('template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">  

  <link rel="stylesheet" href="{{URL('template/bower_components/morris.js/morris.css')}}">
  
  <link rel="stylesheet" href="{{URL('template/bower_components/jvectormap/jquery-jvectormap.css')}}">
  
  <link rel="stylesheet" href="{{URL('template/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  
  <link rel="stylesheet" href="{{URL('template/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">

  <link rel="stylesheet" href="{{URL('template/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

  <link rel="stylesheet" href="{{URL('css/sweetalert.css')}}">
  
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition sidebar-mini">

<!-- <div class="wrapper"> -->

<header class="main-header">
  <a href="/" class="logo">
    <span class="logo-mini"><b>Persuratan</b></span>
    <span class="logo-lg"><b>Persuratan</b></span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">    
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>    
  </nav> 
</header>

<!-- Menu sidebar -->
<aside class="main-sidebar">
  <div class="user-panel">
    <div class="image">
      <img src="images/avatar2.png" class="img-circle center user-image" alt="User Image">      
      <div class="username">{{$userLogin->name}}</div>
      <div class="username" id="role">        
        @if($userLogin->role == "Kepala Bidang")
          {{$userLogin->role}} {{$userLogin->department->name}}
        @elseif($userLogin->role == "Kepala Sub Bagian")
          {{$userLogin->role}} {{$userLogin->division->name}}
        @elseif($userLogin->role == "Kasie")
          {{$userLogin->role}} {{$userLogin->section->name}}
        @elseif($userLogin->role == "Staff")
          @if($userLogin->section_id != null)
              {{$userLogin->role}} {{$userLogin->section->name}}
          @elseif($userLogin->division_id != null)
              {{$userLogin->role}} {{$userLogin->division->name}}
          @endif
        @else
          {{$userLogin->role}}
        @endif
      </div>
    </div>      
    <div class="col col-sm-6 add-on-btn" >
      <a href="#" id="profil">
        <i class="fa fa-pencil"></i> Edit Profil
      </a>
    </div>
    <div class="col col-sm-6 add-on-btn" >      
      <a id="logout" onclick="logout()">
        <i class="fa fa-power-off"></i> Log Out
      </a>
    </div>
    <form action="{{Route('logout')}}" method="POST" id="logout-form" style="display:none">
      {{csrf_field()}}
    </form>
  </div>
  <section class="sidebar">      
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN MENU</li>
      @yield('sidebar-menu')
    </ul>
  </section>
</aside>

<div class="content-wrapper">
  
  <script src="{{URL('template/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{URL('template/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{URL('template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
  <script src="{{URL('template/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{URL('template/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{URL('template/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>  
  @include('sweetalert::alert')
  @yield('content')
</div>

<!-- <footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.4.0
  </div>
  <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
  reserved.
</footer> -->

<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<script src="{{URL('template/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{URL('template/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{URL('template/bower_components/morris.js/morris.min.js')}}"></script>

<script src="{{URL('template/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>

<script src="{{URL('template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{URL('template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

<script src="{{URL('template/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>

<script src="{{URL('template/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{URL('template/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>


<script src="{{URL('template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>

<script src="{{URL('template/bower_components/fastclick/lib/fastclick.js')}}"></script>

<script src="{{URL('template/dist/js/adminlte.min.js')}}"></script>
</body>

<script>
  function logout(){
    event.preventDefault();
    document.getElementById('logout-form').submit();
  }
</script>

</html>
