<?php

use Faker\Generator as Faker;

$factory->define(App\Models\index::class, function (Faker $faker) {
    return [
        'code' => $faker->numberBetween(100,900),
        'name'  => $faker->words(2,true),
        'information' => sentence()
    ];
});
