<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_mails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mail_code');
            $table->string('mail_no');
            $table->string('sender');
            $table->date('mail_date');
            $table->string('contact');
            $table->string('mail_subject');
            $table->string('mail_info');
            $table->boolean('completion_status')->default(false);
            $table->string('intended_disposition')->nullable();
            $table->text('jobdesc')->nullable();
            
            $table->unsignedInteger('index_id');            
            $table->foreign('index_id')->references('id')->on('indices'); 
            
            $table->boolean('status_kadin')->default(false);
            $table->boolean('status_sekre')->default(false);
            $table->boolean('status_department')->default(false);
            $table->boolean('status_division')->default(false);
            $table->boolean('status_section')->default(false);
            
            // $table->unsignedInteger('user_id')->nullable();
            // $table->foreign('user_id')->references('id')->on('users');

            // $table->unsignedInteger('department_id')->nullable();
            // $table->foreign('department_id')->references('id')->on('departments');

            // $table->unsignedInteger('division_id')->nullable();
            // $table->foreign('division_id')->references('id')->on('divisions');

            // $table->unsignedInteger('section_id')->nullable();
            // $table->foreign('section_id')->references('id')->on('sections');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_mails');
    }
}
