<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::insert([
            [
                'name'  => 'Admin',
                'username'   => 'admin',
                'password'   => bcrypt('1234'),
                'role'       => 'Admin',
                'department_id' => null,
                'division_id' => null,
                'section_id' => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'Kepala Dinas',
                'username'   => 'kadinas',
                'password'   => bcrypt('1234'),
                'role'       => 'Kepala Dinas',
                'department_id' => null,
                'division_id' => null,
                'section_id' => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'Sekretaris',
                'username'   => 'sekretaris',
                'password'   => bcrypt('1234'),
                'role'       => 'Sekretaris',
                'department_id' => null,
                'division_id' => null,
                'section_id' => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'umpeg',
                'username'   => 'umpeg',
                'password'   => bcrypt('1234'),
                'role'       => 'Kepala Sub Bagian',                
                'department_id' => null,
                'division_id' => '1',
                'section_id' => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'keuangan',
                'username'   => 'keuangan',
                'password'   => bcrypt('1234'),
                'role'       => 'Kepala Sub Bagian',
                'department_id' => null,
                'division_id' => '2',
                'section_id' => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'pembinaan SD',
                'username'   => 'pembinaansd',
                'password'   => bcrypt('1234'),
                'role'       => 'Kepala Bidang',
                'department_id' => '1',
                'division_id' => null,
                'section_id' => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'kurikulum SD',
                'username'   => 'kurikulumsd',
                'password'   => bcrypt('1234'),
                'role'       => 'Kasie',
                'department_id' => null,
                'division_id' => null,
                'section_id' => '1',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'Operator',
                'username'   => 'op',
                'password'   => bcrypt('1234'),
                'role'       => 'Operator Surat',
                'department_id' => null,
                'division_id' => null,
                'section_id' => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
        ]);
    }
}
