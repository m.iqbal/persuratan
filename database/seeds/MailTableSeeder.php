<?php

use Illuminate\Database\Seeder;

class MailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\incoming_mail::insert([
            [
                'mail_code'	=> '1001',
                'mail_no' => '11/12/xx',
                'sender'  => 'Kesbangpol',
                'mail_date' => \Carbon\Carbon::createFromDate(2018,11,8)->toDateTimeString(),
                'contact' => 'kontak',
                'mail_subject' => 'permohonan',
                'mail_info' => 'keterangan',
                'completion_status' => false,
                'intended_disposition' => null,
                'index_id' => '1',
                'jobdesc'   => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'mail_code'	=> '1002',
                'mail_no' => '11/21/xii',
                'sender'  => 'SDN 1 Sleman',
                'mail_date' => \Carbon\Carbon::createFromDate(2018,11,9)->toDateTimeString(),
                'contact' => 'kontak',
                'mail_subject' => 'pengajuan',
                'mail_info' => 'keterangan',
                'completion_status' => false,
                'intended_disposition' => null,
                'index_id' => '2',
                'jobdesc'   => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'mail_code'	=> '1003',
                'mail_no' => '11/23/xii',
                'sender'  => 'SDN 2 Sleman',
                'mail_date' => \Carbon\Carbon::createFromDate(2018,11,10)->toDateTimeString(),
                'contact' => 'kontak',
                'mail_subject' => 'undangan',
                'mail_info' => 'keterangan',
                'completion_status' => false,
                'intended_disposition' => null,
                'index_id' => '2',
                'jobdesc'   => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'mail_code'	=> '1004',
                'mail_no' => '11/24/xii',
                'sender'  => 'SDN 2 Sleman',
                'mail_date' => \Carbon\Carbon::createFromDate(2018,11,11)->toDateTimeString(),
                'contact' => 'kontak',
                'mail_subject' => 'cuti',
                'mail_info' => 'keterangan',
                'completion_status' => false,
                'intended_disposition' => null,
                'index_id' => '3',
                'jobdesc'   => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'mail_code'	=> '1005',
                'mail_no' => '11/24/xii',
                'sender'  => 'Kesbangpol',
                'mail_date' => \Carbon\Carbon::createFromDate(2018,11,12)->toDateTimeString(),
                'contact' => 'kontak',
                'mail_subject' => 'permohonan narasumber',
                'mail_info' => 'keterangan',
                'completion_status' => false,
                'intended_disposition' => null,
                'index_id' => '1',
                'jobdesc'   => null,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')
            ],
        ]);
    }
}
