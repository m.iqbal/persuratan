<?php

use Illuminate\Database\Seeder;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\division::insert([
            [
                'name'  => 'Umum Kepegawaian',
                'information'   => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'name'  => 'Keuangan',
                'information'   => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'name'  => 'Perencanaan',
                'information'   => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],
        ]);
    }
}
