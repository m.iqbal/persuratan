<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\department::insert([
            [
                'name'  => 'Pembinaan SD',
                'information' => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'Pembinaan SMP',
                'information' => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'PAUD',
                'information' => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'Sarpras',
                'information' => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],
        ]);
    }
}
