<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\section::insert([
            [
                'name'  => 'kurikulum SD',
                'information'   => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [
                'name'  => 'kurikulum SMP',
                'information'   => 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],
        ]);
    }
}
