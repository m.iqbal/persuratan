<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\Models\index;

class IndicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\index::insert([
            [   'code'	=> '101',
                'name'	=> 'Permohonan',
                'information'	=> 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            [   'code'	=> '102',
                'name'	=> 'Pengajuan',
                'information'	=> 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
                'code'	=> '103',
                'name'	=> 'Cuti',
                'information'	=> 'ket',
                'created_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],
        ]);
    }
}
