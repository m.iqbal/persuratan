<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

Route::group(['middleware'=>'auth'], function(){
    Route::get('/','UserController@getDashboard'); 
});

// admin kelola user
Route::get('user', 'UserController@get');
Route::post('user', 'UserController@store');
Route::put('user/{id}/edit', 'UserController@update');
Route::delete('user/{id}/delete', 'UserController@destroy');

    // admin kelola department
    Route::get('department','DepartmentController@get');
    Route::post('department', 'DepartmentController@store');
    Route::put('department/{id}/edit', 'DepartmentController@update');
    Route::delete('department/{id}/delete', 'DepartmentController@destroy');

// admin kelola division
Route::get('division','DivisionController@get');
Route::post('division', 'DivisionController@store');
Route::put('division/{id}/edit', 'DivisionController@update');
Route::delete('division/{id}/delete', 'DivisionController@destroy');

    // admin kelola section
    Route::get('section','SectionController@get');
    Route::post('section','SectionController@store');
    Route::put('section/{id}/edit','SectionController@update');
    Route::delete('section/{id}/delete','SectionController@destroy');

// admin kelola index
Route::get('index','IndexController@get');
Route::post('index','IndexController@store');
Route::put('index/{id}/edit','IndexController@update');
Route::delete('index/{id}/delete','IndexController@destroy');

    //operator surat masuk
    Route::get('surat-masuk', 'MailController@get');
    Route::post('surat-masuk', 'MailController@store');
    Route::put('surat-masuk/{id}/edit', 'MailController@update');
    Route::delete('surat-masuk/{id}/delete', 'MailController@destroy');

//umpeg surat masuk
Route::get('umpeg-surat-masuk', 'MailController@get2');
Route::put('umpeg-surat-masuk/{id}/dispo', 'MailController@dispo');

    //kadinas sekre umpeg subag kabid kasi suratku
    Route::get('suratku', 'MailController@getSuratku');
    Route::put('add-jobdesc/{id}/add', 'MailController@jobdesc');
    Route::put('selesaiDispo/{id}', 'MailController@completionDispo');

// Route::get('/home', 'HomeController@index')->name('home');